import React from "react";
import "./App.scss";
import ColorBox from "./components/ColorBox";

function App() {
  return (
    <div className="app">
      <h1>Hello</h1>
      <ColorBox />
    </div>
  );
}

export default App;
