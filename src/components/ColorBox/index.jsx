// #rsfp

import React, { useState } from "react";
import PropTypes from "prop-types";

ColorBox.propTypes = {};

function ColorBox() {
  const [color, setColor] = useState("deeppink");

  handleBoxClick = () => {
    //get random color --> set color
    const newColor = getRandomColor();
    setColor;
  };

  return (
    <div className="color-box" style={{ backgroundColor: color }} onClick={}>
      COLOR BOX
    </div>
  );
}

export default ColorBox;
